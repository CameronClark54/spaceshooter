#pragma once
#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <vector>
#include "Bullet.h"

class Enemy
{
public: //access level

//Constructor
	Enemy(sf::Texture& playerTexture,
		sf::Vector2u newScreenSize,
		std::vector<Bullet>& newBullets,
		sf::Texture&newBulletTexture,
		sf::SoundBuffer& firingSoundBuffer,
		sf::Vector2f startingPosition,
		std::vector<sf::Vector2f> newMovementPattern);

	//functions to call player code
	void Update(sf::Time frameTime);
	
	void Draw(sf::RenderWindow& gameWindow);

	// Getters
	bool GetAlive();
	sf::FloatRect GetHitBox();

	//Setters
	void SetAlive(bool newAlive);

private:

	//Variables used by player class
	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;
	sf::Vector2u screenSize;
	std::vector<Bullet>* bullets;
	sf::Texture* bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletFireSound;
	std::vector<sf::Vector2f> movementPattern;
	int currentInstruction;
	bool alive;
};

