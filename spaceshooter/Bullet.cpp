#include "Bullet.h"

Bullet::Bullet(sf::Texture & bulletTexture,
	sf::Vector2u newScreenSize,
	sf::Vector2f startingPosition,
	sf::Vector2f newVelocity)
	:screenSize(newScreenSize),
	sprite(bulletTexture),
	velocity(newVelocity)
{
	
	sprite.setPosition(startingPosition);
	sprite.setRotation(90.0f);
	bool alive = true;

}

void Bullet::Update(sf::Time frameTime)
{
	sf::Vector2f newPosition = sprite.getPosition() + velocity* frameTime.asSeconds();
	//check if bullet is off screen
	if (newPosition.x + sprite.getTexture()->getSize().x < 0 || newPosition.x > screenSize.x) 
	{
		alive = false;
		//delete bullet when off screen
		
	}

	//set new position
	sprite.setPosition(newPosition);
}

void Bullet::Draw(sf::RenderWindow & gameWindow)
{
	gameWindow.draw(sprite);
}

bool Bullet::GetAlive()
{
	return alive;
}

sf::FloatRect Bullet::GetHitBox()
{
	return sprite.getGlobalBounds();
}

void Bullet::SetAlive(bool newAlive)
{
	alive = newAlive;
}
