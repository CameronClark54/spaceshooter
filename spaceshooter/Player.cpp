#include "Player.h"
#include "Bullet.h"


Player::Player(sf::Texture & playerTexture, sf::Vector2u newScreenSize, std::vector<Bullet>& newBullets, sf::Texture&newBulletTexture, sf::SoundBuffer&firingSoundBuffer):sprite(playerTexture),velocity(0.0f,0.0f),speed(200.0f),screenSize(newScreenSize),bullets(newBullets),bulletTexture(newBulletTexture), bulletCooldownRemaining(sf::seconds(0.0f)), bulletCooldownMax(sf::seconds(0.5f)), bulletFireSound(firingSoundBuffer)
{
	
	sprite.setPosition((screenSize.x / 2 - playerTexture.getSize().x / 2)*0.25f, screenSize.y / 2 - playerTexture.getSize().y / 2);
	sprite.setRotation(90.0f);
	
	Reset();

}

void Player::Input()
{
	//stops the player moving when no key is pressed
	velocity.x = 0.0f;
	velocity.y = 0.0f;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		// moves player up
		velocity.y = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// moves player left
		velocity.x = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		// moves player down
		velocity.y = speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// moves player right
		velocity.x = speed;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)&& bulletCooldownRemaining <= sf::seconds(0.0f))
	{
		sf::Vector2f bulletPosition = sprite.getPosition();
		bulletPosition.y += sprite.getTexture()->getSize().y / 2 - bulletTexture.getSize().y / 2;
		bulletPosition.x += sprite.getTexture()->getSize().x / 2 - bulletTexture.getSize().x / 2;
		bullets.push_back(Bullet(bulletTexture, screenSize, bulletPosition, sf::Vector2f(1000, 0)));
		// Play firing sound
		bulletFireSound.play();

		// reset bullet cooldown
		bulletCooldownRemaining = bulletCooldownMax;
	}

}

void Player::Update(sf::Time frameTime)
{
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();

	//check if the left of the sprite is off the screen to the left
	if (newPosition.x < 0) 
	{
		newPosition.x = 0;
	}

	//Check if the right side of the sprite is off the right side of the screen
	if (newPosition.x+sprite.getTexture()->getSize().x > screenSize.x)
	{
		newPosition.x = screenSize.x - sprite.getTexture()->getSize().x;
	}

	//check if the top of the sprite is off the screen to the top
	if (newPosition.y < 0)
	{
		newPosition.y = 0;
	}

	//check if the bottom of the sprite is off the screen to the bottom
	if (newPosition.y + sprite.getTexture()->getSize().y > screenSize.y)
	{
		newPosition.y = screenSize.y - sprite.getTexture()->getSize().y;
	}

	//updates player possition
	sprite.setPosition(sprite.getPosition() + velocity * frameTime.asSeconds());

	// Update the cooldown remaining for firing bullets
	bulletCooldownRemaining -= frameTime;
}

void Player::Reset()
{
	sprite.setPosition(screenSize.x / 2 - sprite.getTexture()->getSize().x / 2, screenSize.y / 2 - sprite.getTexture()->getSize().y / 2);
}

void Player::Draw(sf::RenderWindow& gameWindow)
{
	gameWindow.draw(sprite);
}

sf::FloatRect Player::GetHitBox()
{
	return sprite.getGlobalBounds();
}
