#pragma once
#include <SFML/Graphics.hpp>
#include <vector>

class Bullet
{
public:
	//constructor
	Bullet(sf::Texture& bulletTexture, sf::Vector2u newScreenSize, sf::Vector2f startingPosition, sf::Vector2f newVelocity);

	//functions
	void Update(sf::Time frameTime);
	void Draw(sf::RenderWindow & gameWindow);

	// Getters
	bool GetAlive();
	sf::FloatRect GetHitBox();

	//Setters
	void SetAlive(bool newAlive);

private:
	// Variables
	sf::Sprite sprite;
	sf::Vector2f velocity;
	sf::Vector2u screenSize;
	bool alive;
};

