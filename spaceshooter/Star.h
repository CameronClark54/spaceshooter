#pragma once
#include <SFML/Graphics.hpp>
#include <vector>

class Star
{
public: //access level

//Constructor
	Star(sf::Texture& starTexture, sf::Vector2u screenSize);

	//functions to call code
	void Update(sf::Time frameTime);
	void Reset();
	void Draw(sf::RenderWindow& gameWindow);

private:

	//Variables used by  class
	sf::Sprite sprite;
	float speed;
	sf::Vector2u screenSize;
};

