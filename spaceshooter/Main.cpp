#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include <vector>
#include <cstdlib>
#include <time.h>

#include "Player.h"
#include "Star.h"
#include "Bullet.h"
#include "Enemy.h"
#include "SpawnData.h"

int main()
{
	// declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Space Shooter", sf::Style::Titlebar | sf::Style::Close);

	//////////////////////////////////////////////////////
	//GAME SETUP
	//////////////////////////////////////////////////////


	// Bullets setup
	std::vector<Bullet> playerBullets;
	std::vector<Bullet> enemyBullets;
	sf::Texture playerBulletTexture;
	playerBulletTexture.loadFromFile("Assets/Graphics/playerBullet.png");
	sf::Texture enemyBulletTexture;
	enemyBulletTexture.loadFromFile("Assets/Graphics/enemyBullet.png");

	// Player setup
	sf::Texture playerTexture;
	// load our texture using a file path
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	//add sound
	sf::SoundBuffer firingSoundBuffer;
	firingSoundBuffer.loadFromFile("Assets/Audio/fire.ogg");
	//declare player object
	Player playerObject(playerTexture,
		gameWindow.getSize(),
		playerBullets,
		playerBulletTexture,
		firingSoundBuffer);
	sf::Sprite playerSprite;

	//movement patterns
	std::vector<sf::Vector2f> zigZagPattern;
	zigZagPattern.push_back(sf::Vector2f(900, 900));
	zigZagPattern.push_back(sf::Vector2f(600, 300));
	zigZagPattern.push_back(sf::Vector2f(300, 900));

	// enemy setup
	std::vector<Enemy>enemies;

	sf::Texture enemyTexture;
	// load our texture using a file path
	enemyTexture.loadFromFile("Assets/Graphics/enemy.png");

	//create enemies using a vector
	enemies.push_back(Enemy(enemyTexture,
		gameWindow.getSize(),
		enemyBullets,
		enemyBulletTexture,
		firingSoundBuffer,
		sf::Vector2f(gameWindow.getSize().x - 100, gameWindow.getSize().y / 2),
		zigZagPattern));
	
	//spawn info
	std::vector<SpawnData>spawnData;
	int spawnIndex = 0; //tells us which spawn we are on
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x,100),
		zigZagPattern,
		sf::seconds(1.0f) });
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x,500),
		zigZagPattern,
		sf::seconds(1.0f) });
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x,250),
		zigZagPattern,
		sf::seconds(1.0f) });
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x,750),
		zigZagPattern,
		sf::seconds(1.0f) });


	sf::Time timeToSpawn = spawnData[0].delay;



	//Stars setup
	sf::Texture starTexture;
	starTexture.loadFromFile("Assets/Graphics/star.png");
	std::vector<Star> stars;
	int numStars = 5;
	for (int i = 0; i < numStars; ++i) 
	{
		stars.push_back(Star(starTexture, gameWindow.getSize()));
	}



	// Game Music
	sf::Music gameMusic;
	// load our audio using a file path
	gameMusic.openFromFile("Assets/Audio/music.ogg");
	// start the music
	gameMusic.play();

	// Game Font
	sf::Font gameFont;
	// load our font using a file path
	gameFont.loadFromFile("Assets/Font/mainFont.ttf");

	// Score
	// Declare an integer variable to hold our numerical score value
	int score = 0;
	// Setup score text object
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(40);
	scoreText.setFillColor(sf::Color::Red);
	scoreText.setPosition(30, 30);


	//Timer
	sf::Clock gameClock;

	//Player movement
	sf::Vector2f playerVelocity(0.0f, 0.0f);
	float speed = 100.0f;

	//Set game over variable
	bool gameOver = false;

	// Game Over Text
	// Declare a text variable called gameOverText to hold our game over display
	sf::Text gameOverText;
	// Set the font our text should use
	gameOverText.setFont(gameFont);
	// Set the string of text that will be displayed by this text object
	gameOverText.setString("GAME OVER\n\npress R to restart\nor Q to quit");
	// Set the size of our text, in pixels
	gameOverText.setCharacterSize(72);
	// Set the colour of our text
	gameOverText.setFillColor(sf::Color::Cyan);
	// Set the text style for the text
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	// Position our text in the top center of the screen
	gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);

	sf::Text winText;
	// Set the font our text should use
	winText.setFont(gameFont);
	// Set the string of text that will be displayed by this text object
	winText.setString("YOU WIN\n\npress R to restart\nor Q to quit");
	// Set the size of our text, in pixels
	winText.setCharacterSize(72);
	// Set the colour of our text
	winText.setFillColor(sf::Color::Cyan);
	// Set the text style for the text
	winText.setStyle(sf::Text::Bold | sf::Text::Italic);
	// Position our text in the top center of the screen
	winText.setPosition(gameWindow.getSize().x / 2 - winText.getLocalBounds().width / 2, 150);

	//win state
	bool winState = false;

	//loss/win jingles
	sf::SoundBuffer lossBuffer;
	lossBuffer.loadFromFile("Assests/Audio/loss.ogg");
	sf::Sound lossSFX(lossBuffer);

	sf::SoundBuffer winBuffer;
	winBuffer.loadFromFile("Assests/Audio/win.ogg");
	sf::Sound winSFX(winBuffer);

	

	//Game Loop
	while (gameWindow.isOpen())
	{
		//////////////////////////////////////////////////////////
		// Input Section
		//////////////////////////////////////////////////////////

		//declare a variable to hold an event
		sf::Event gameEvent;

		while (gameWindow.pollEvent(gameEvent))
		{
			//check if the player has tried to close the game window
			if (gameEvent.type == sf::Event::Closed)
			{
				//if true then close the game window
				gameWindow.close();

			}//end if


		}//end loop

		//player keyboard input
		playerObject.Input();




		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			gameWindow.close();
		}

		if (gameOver || winState) 
		{

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
			{
				gameWindow.close();
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
			{
				gameOver = false;
				winState = false;
				playerObject.Reset();
				score = 0;
				gameMusic.play();
				enemies.clear();
				playerBullets.clear();
				enemyBullets.clear();
				spawnIndex = 0;
			}
		}




		////////////////////////////////////////////////////////////////
		//Update game Section
		////////////////////////////////////////////////////////////////

		//Update game state
		sf::Time frameTime = gameClock.restart();



		if (!gameOver && !winState)
		{
			
			//Update score
			scoreText.setString("Score: " + std::to_string(score));

			//Update stars
			for (int i = 0; i < stars.size(); ++i)
			{
				stars[i].Update(frameTime);
			}

			//Update bullets
			for (int i = playerBullets.size() - 1; i >= 0; --i)
			{
				playerBullets[i].Update(frameTime);

				//if bullet is off screen then delete it
				if (!playerBullets[i].GetAlive())
				{
					// Remove the item from the vector
					playerBullets.erase(playerBullets.begin() + i);
				}
			}

			for (int i = enemyBullets.size() - 1; i >= 0; --i)
			{
				enemyBullets[i].Update(frameTime);

				//if bullet is off screen then delete it
				if (!enemyBullets[i].GetAlive())
				{
					// Remove the item from the vector
					enemyBullets.erase(enemyBullets.begin() + i);
				}
			}

			//move player sprite
			playerObject.Update(frameTime);

			//move enemy sprites
			for (int i = enemies.size() - 1; i >= 0; --i)
			{
				enemies[i].Update(frameTime);

				//if enemy is dead then delete it
				if (!enemies[i].GetAlive())
				{
					// Remove the item from the vector
					enemies.erase(enemies.begin() + i);
				}
			}

			//check if it is time to spawn a new enemy
			timeToSpawn -= frameTime;
			if (timeToSpawn <= sf::seconds(0) && spawnIndex < spawnData.size())
			{
				//spawn an enemy at the position in the pattern
				enemies.push_back(Enemy(enemyTexture,
					gameWindow.getSize(),
					enemyBullets,
					enemyBulletTexture,
					firingSoundBuffer,
					spawnData[spawnIndex].position,
					spawnData[spawnIndex].pattern));

				++spawnIndex;
				if (spawnIndex < spawnData.size())
				{
					timeToSpawn = spawnData[spawnIndex].delay;
				}
			}

			//check for collisions between player bullets and enemies
			for (int i = 0; i < playerBullets.size(); ++i)
			{
				for (int j = 0; j < enemies.size(); ++j)
				{
					sf::FloatRect bulletBounds = playerBullets[i].GetHitBox();
					sf::FloatRect enemyBounds = enemies[j].GetHitBox();

					if (bulletBounds.intersects(enemyBounds))
					{
						//they intersect so delete them
						playerBullets[i].SetAlive(false);
						enemies[j].SetAlive(false);
						score++;


					}
				}

			}


			//check for collisions between enemies bullets and the player
			for (int i = 0; i < enemyBullets.size(); ++i)
			{

				sf::FloatRect bulletBounds = enemyBullets[i].GetHitBox();
				sf::FloatRect playerBounds = playerObject.GetHitBox();

				if (bulletBounds.intersects(playerBounds))
				{
					//they intersect so game over
					gameOver = true;
					gameMusic.stop();
					lossSFX.play();

				}


			}

			//check if we have won, we win if pattern is over and all enemies are gone
			if (spawnIndex >= spawnData.size() && enemies.empty()) 
			{
				winState = true;
				gameMusic.stop();
				winSFX.play();
			}

		}



		

		////////////////////////////////////////////////////////////////////////////
		//Draw section
		////////////////////////////////////////////////////////////////////////////

		//clears the window to a single colour
		gameWindow.clear(sf::Color::Black);

		if (!gameOver && !winState) 
		{
			//draws player
			playerObject.Draw(gameWindow);

			//draws enemies
			for (int i = 0; i < enemies.size(); ++i)
			{
				enemies[i].Draw(gameWindow);
			}

			//draws bullets
			for (int i = 0; i < playerBullets.size(); ++i)
			{
				playerBullets[i].Draw(gameWindow);
			}

			for (int i = 0; i < enemyBullets.size(); ++i)
			{
				enemyBullets[i].Draw(gameWindow);
			}
		}




		//draws stars
		for (int i = 0; i < stars.size(); ++i) 
		{
			stars[i].Draw(gameWindow);
		}



		gameWindow.draw(scoreText);

		if (gameOver) 
		{
			gameWindow.draw(gameOverText);
		}

		if (winState) 
		{
			gameWindow.draw(winText);
		}
		

		// displays the windows contents onto the screen
		gameWindow.display();
	}//end of game loop

	return 0;
}