#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <vector>
#include "Bullet.h"


class Player
{
public: //access level

	//Constructor
	Player(sf::Texture& playerTexture, sf::Vector2u newScreenSize, std::vector<Bullet>& newBullets, sf::Texture&newBulletTexture, sf::SoundBuffer& firingSoundBuffer);

	//functions to call player code
	void Input();
	void Update(sf::Time frameTime);
	void Reset();
	void Draw(sf::RenderWindow& gameWindow);

	//Getters
	sf::FloatRect GetHitBox();



private:

	//Variables used by player class
	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;
	sf::Vector2u screenSize;
	std::vector<Bullet>& bullets;
	sf::Texture& bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletFireSound;
};