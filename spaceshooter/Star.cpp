#include "Star.h"
#include <cstdlib>

Star::Star(sf::Texture & starTexture, sf::Vector2u newScreenSize):screenSize(newScreenSize),sprite(starTexture),speed(400.0f)
{

	Reset();
}

void Star::Update(sf::Time frameTime)
{
	//set new position
	sf::Vector2f newPosition = sprite.getPosition() +sf::Vector2f(-speed,0)* frameTime.asSeconds();

	//checks if sprite has went off screen
	if (newPosition.x + sprite.getTexture()->getSize().x < 0) 
	{
		//wrap sprite to right hand side of screen
		newPosition.x = screenSize.x;
	}
	//set new position
	sprite.setPosition(newPosition);
}

void Star::Reset()
{
	//set rand start position
	sprite.setPosition(sf::Vector2f(rand() % screenSize.x, rand() % screenSize.y));

	//set rand sprite size (from normal size to 1/4th size)
	float scaleFactor = 1.0f / (1.0f + rand() % 4);
	sprite.setScale(scaleFactor, scaleFactor);
}

void Star::Draw(sf::RenderWindow & gameWindow)
{
	gameWindow.draw(sprite);
}
