#include "Enemy.h"
#include "Bullet.h"

Enemy::Enemy(sf::Texture & playerTexture,
	sf::Vector2u newScreenSize,
	std::vector<Bullet>& newBullets,
	sf::Texture & newBulletTexture,
	sf::SoundBuffer & firingSoundBuffer,
	sf::Vector2f startingPosition,
	std::vector<sf::Vector2f> newMovementPattern)
	:sprite(playerTexture), 
	velocity(0.0f, 0.0f), 
	speed(200.0f), 
	screenSize(newScreenSize), 
	bullets(&newBullets), 
	bulletTexture(&newBulletTexture),
	bulletCooldownRemaining(sf::seconds(0.0f)), 
	bulletCooldownMax(sf::seconds(0.5f)),
	bulletFireSound(firingSoundBuffer),
	movementPattern(newMovementPattern),
	currentInstruction(0),
	alive(true)
{
	sprite.setPosition(startingPosition);
	sprite.setRotation(90.0f);

	
}

void Enemy::Update(sf::Time frameTime)
{
	//if we reach end of pattern, do nothing
	if (currentInstruction >= movementPattern.size())
	{
		//enemy should delete itself when it finishes its movement
		alive = false;
		//bail
		return;
	}
	//get target from instruction
	sf::Vector2f targetPoint=movementPattern[currentInstruction];

	//get distance vector from target point
	sf::Vector2f distanceVector = targetPoint - sprite.getPosition();

	//calc direction vector by dividing distance vector by magnitude(length)
	float distanceMag = std::sqrt(distanceVector.x*distanceVector.x + distanceVector.y*distanceVector.y);
	sf::Vector2f directionVector = distanceVector / distanceMag;

	float distanceToTravel = speed * frameTime.asSeconds();

	//updates enemy possition
	//old position + direction*distance travelled in that direction
	//(just scalling our direction by how far we travelled in that direction)
	sf::Vector2f newPosition = sprite.getPosition() + directionVector * distanceToTravel;

	//check if we will reach or overshoot target
	//we will reach/overshoot if the distance to the target is less than the distance travelled
	if (distanceMag <= distanceToTravel) 
	{
		//we have reched the target
		//set position so we dont overshoot
		newPosition = targetPoint;

		//start moving to next target
		++currentInstruction;

	}

	sprite.setPosition(newPosition);

	// Update the cooldown remaining for firing bullets
	bulletCooldownRemaining -= frameTime;

	//emeny shooting
	if (bulletCooldownRemaining <= sf::seconds(0.0f))
	{
		sf::Vector2f bulletPosition = sprite.getPosition();
		bulletPosition.y += sprite.getTexture()->getSize().y / 2 - bulletTexture->getSize().y / 2;
		bulletPosition.x += sprite.getTexture()->getSize().x / 2 - bulletTexture->getSize().x / 2;
		bullets->push_back(Bullet(*bulletTexture, screenSize, bulletPosition, sf::Vector2f(-1000, 0)));
		// Play firing sound
		bulletFireSound.play();

		// reset bullet cooldown
		bulletCooldownRemaining = bulletCooldownMax;
	}
}

void Enemy::Draw(sf::RenderWindow & gameWindow)
{
	gameWindow.draw(sprite);
}

bool Enemy::GetAlive()
{
	return alive;
}

sf::FloatRect Enemy::GetHitBox()
{
	return sprite.getGlobalBounds();
}

void Enemy::SetAlive(bool newAlive)
{
	alive = newAlive;
}
